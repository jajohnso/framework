#include "SimpleAnalysisFramework/MVA.h"
#include "SimpleAnalysisFramework/AnalysisClass.h"

TMVAReader::TMVAReader(const std::string &name,
                       const std::vector<std::string> &variableDefs,
                       const std::string fname1, const std::string fname2)
    : MVA(name, variableDefs) {

  // This loads the library
  TMVA::Tools::Instance();

  // Initialize reader(s)
  m_bdt1 = new TMVA::Reader("!Color:Silent");
  if (fname2 != "")
    m_bdt2 = new TMVA::Reader("!Color:Silent");
  else
    m_bdt2 = 0;

  m_variables.resize(variableDefs.size(), 0);

  int idx = 0;
  for (auto label : variableDefs) {
    std::string var = label + " := " + label;
    m_bdt1->AddVariable(var, &m_variables[idx]);
    if (m_bdt2)
      m_bdt2->AddVariable(var, &m_variables[idx]);
    idx++;
  }

  m_bdt1->BookMVA(name, FindFile(fname1));
  if (m_bdt2)
    m_bdt2->BookMVA(name, FindFile(fname2));
}

double TMVAReader::evaluate(const std::vector<double> &values,
                            const std::string /* nodeName */) {
  TMVA::Reader *bdt = m_bdt1;
  if (m_bdt2 && ((m_eventNumber % 2) == 1))
    bdt = m_bdt2;

  if (values.size() != m_variables.size())
    throw std::runtime_error("Wrong number of variables into TMVAReader");
  for (size_t ii = 0; ii < values.size(); ii++)
    m_variables[ii] = values[ii];
  return bdt->EvaluateMVA(m_name);
}

MVAUtilsReader::MVAUtilsReader(const std::string &name,
                               const std::string fname1,
                               const std::string fname2)
    : MVA(name, {}) {
  TFile *f1 = TFile::Open(FindFile(fname1).c_str(), "READ");
  TTree *tree1 = nullptr;
  f1->GetObject(name.c_str(), tree1);
  if (tree1 == nullptr)
    throw std::runtime_error("Did not find MVA tree");
  m_bdt1 = new MVAUtils::BDT(tree1);
  m_bdt2 = nullptr;
  if (fname2 != "") {
    TFile *f2 = TFile::Open(FindFile(fname2).c_str(), "READ");
    TTree *tree2 = nullptr;
    f2->GetObject(name.c_str(), tree2);
    if (tree2 == nullptr)
      throw std::runtime_error("Did not find MVA tree");
    m_bdt2 = new MVAUtils::BDT(tree2);
  }
}

double MVAUtilsReader::evaluate(const std::vector<double> &values,
                                const std::string /* nodeName */) {
  MVAUtils::BDT *bdt = m_bdt1;
  if (m_bdt2 && ((m_eventNumber % 2) == 1))
    bdt = m_bdt2;
  std::vector<float> floatValues(values.begin(), values.end());

  return bdt->GetResponse(floatValues);
}

std::vector<double>
MVAUtilsReader::evaluateMulti(const std::vector<double> &values,
                              int numClasses) {
  MVAUtils::BDT *bdt = m_bdt1;
  if (m_bdt2 && ((m_eventNumber % 2) == 1))
    bdt = m_bdt2;

  std::vector<float> floatValues(values.begin(), values.end());
  auto results = bdt->GetMultiResponse(floatValues, numClasses);

  std::vector<double> doubleResult(results.begin(), results.end());
  return doubleResult;
}

ONNXReader::ONNXReader(const std::string &name, const std::string fname1,
                       const std::string fname2)
    : MVA(name, {}) {
  m_env = new Ort::Env(ORT_LOGGING_LEVEL_WARNING, name.c_str());
  Ort::SessionOptions sessionOptions;
  sessionOptions.SetIntraOpNumThreads(1);

  m_NN1 = new Ort::Session(*m_env, FindFile(fname1).c_str(), sessionOptions);
  m_NN2 = nullptr;
  if (fname2 != "")
    m_NN2 = new Ort::Session(*m_env, FindFile(fname2).c_str(), sessionOptions);

  Ort::AllocatorWithDefaultOptions allocator;
  m_input_node_names.reserve(m_NN1->GetInputCount());
  for (size_t i = 0; i < m_NN1->GetInputCount(); ++i) {
    m_input_node_names.push_back(m_NN1->GetInputName(i, allocator));
  }

  Ort::TypeInfo type_info = m_NN1->GetInputTypeInfo(
      0); // FIXME: Here we are assuming only one input node
  auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
  m_input_dimension = tensor_info.GetShape();
  // when we have a shape with -1, we need to replace with +1 since it typically
  // refers to batch size
  std::replace_if(
      m_input_dimension.begin(), m_input_dimension.end(),
      [](int64_t &i) { return i == -1; }, 1);

  m_output_node_names.reserve(m_NN1->GetOutputCount());
  for (size_t i = 0; i < m_NN1->GetOutputCount(); ++i)
    m_output_node_names.push_back(m_NN1->GetOutputName(i, allocator));
}

double ONNXReader::evaluate(const std::vector<double> &values,
                            const std::string nodeName) {

  std::vector<float> floatValues(values.begin(), values.end());
  auto output_tensor = _evaluate(floatValues, nodeName);

  return *output_tensor.front().GetTensorMutableData<double>();
}

std::vector<double> ONNXReader::evaluateMulti(const std::vector<double> &values,
                                              int numClasses) {
  auto output_tensor = _evaluate(values, "");

  double *result = output_tensor.front().GetTensorMutableData<double>();

  std::vector<double> output{result, result + numClasses};
  return output;
}

std::vector<float> ONNXReader::evaluateMulti(const std::vector<float> &values,
                                             int numClasses) {
  auto output_tensor = _evaluate(values, "");

  float *result = output_tensor.front().GetTensorMutableData<float>();

  std::vector<float> output{result, result + numClasses};
  return output;
}
